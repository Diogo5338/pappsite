package pt.ipbeja.estig.android.pappsite.model;

public class Folder {

	long id;
	String name;

	public Folder() {
	}

	public Folder(String name) {
		super();
		this.name = name;
	}
	
	public Folder(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

}
