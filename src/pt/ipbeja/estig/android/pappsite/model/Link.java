package pt.ipbeja.estig.android.pappsite.model;

import java.io.ByteArrayOutputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Link {

	long folderId;
	long id;
	String name;
	String url;
	Bitmap image;

	public Link() {
	}

	public Link(String name, String url, long folderId, Bitmap image) {
		this.name = name;
		this.url = url;
		this.folderId = folderId;
		this.image = image;
	}

	public long getFolderId() {
		return folderId;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getUrl() {
		return url;
	}

	public void setFolderId(long folderId) {
		this.folderId = folderId;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public byte[] getImageBytes() {
		try {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			this.image.compress(Bitmap.CompressFormat.PNG, 100, stream);
			byte[] imageByteArray = stream.toByteArray();
			return imageByteArray;
		} catch (Exception e) { return null; }
	}
	
	public Bitmap getImage() {
		return this.image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}
	
	public void setImage(byte[] image) {
		if (image != null) 
			this.image = BitmapFactory.decodeByteArray(image , 0, image.length);
		else
			this.image = null;
	}
	

}
