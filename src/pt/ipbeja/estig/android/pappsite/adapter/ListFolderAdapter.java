package pt.ipbeja.estig.android.pappsite.adapter;

import java.util.List;

import pt.ipbeja.estig.android.pappsite.R;
import pt.ipbeja.estig.android.pappsite.helper.DatabaseHelper;
import pt.ipbeja.estig.android.pappsite.model.Folder;
import pt.ipbeja.estig.android.pappsite.model.Link;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListFolderAdapter extends ArrayAdapter<Folder> {
	private DatabaseHelper db;
	private List<Folder> folders;
	private LayoutInflater inflater;
	private String createFolderString;
	
	
	/**
	 * Folder adapter to be used when we're adding links and folders
	 * 
	 * @param context
	 * @param db
	 * @param createFolderString
	 */
	public ListFolderAdapter(Context context, DatabaseHelper db, String createFolderString) {
		super(context, R.layout.folder_row);
		this.db = db;
		this.inflater = LayoutInflater.from(context);
		this.createFolderString = createFolderString;
		refreshFolders();
		
	}
	
	/**
	 * Folder adapter to list folders in the folder_row.xml in the <b>context</b>
	 * 
	 * @param context
	 * @param db
	 */
	public ListFolderAdapter(Context context, DatabaseHelper db) {
		super(context, R.layout.folder_row);
		this.db = db;
		this.inflater = LayoutInflater.from(context);
		this.createFolderString = "";
		refreshFolders();
	}
	
	public List<Folder> getAllFolders() {
		return folders;
	}

	public void refreshFolders() {
		folders = db.getAllFolders();
		this.clear();
		if (!this.createFolderString.equals("")) 
				this.add(new Folder(this.createFolderString));
		for (Folder f : folders)
			this.add(f);
		this.notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		if (this.createFolderString.equals(""))
			return folders.size();
		else return folders.size()+1;
	}

	static class ViewHolder {
		public ImageView imageIcon;
		public TextView folderName;
		public ImageView linkImage1;
		public ImageView linkImage2;
		public ImageView linkImage3;
		public ImageView linkImage4;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {

		ViewHolder holder;

		if (view == null) {
			view = inflater.inflate(R.layout.folder_row, viewGroup, false);
			holder = new ViewHolder();
			holder.imageIcon = (ImageView) view
					.findViewById(R.id.folderRowImage);
			holder.folderName = (TextView) view
					.findViewById(R.id.folderRowTextView);
			holder.linkImage1 = (ImageView) view
					.findViewById(R.id.linkImage1);
			holder.linkImage2 = (ImageView) view
					.findViewById(R.id.linkImage2);
			holder.linkImage3 = (ImageView) view
					.findViewById(R.id.linkImage3);
			holder.linkImage4 = (ImageView) view
					.findViewById(R.id.linkImage4);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.imageIcon.setImageResource(R.drawable.folder_icon);
		holder.folderName.setText(getItem(i).getName());
		List<Link> links = db.getAllLinksByFolder(getItem(i).getId());
		if (links.size() != 0)
			for (int l = 0; l < links.size(); l++) {
				switch (l) {
				case 0:
					holder.linkImage1.setImageBitmap(links.get(0).getImage());
					break;
				case 1:
					holder.linkImage2.setImageBitmap(links.get(1).getImage());
					break;
				case 2:
					holder.linkImage3.setImageBitmap(links.get(2).getImage());
					break;
				case 3:
					holder.linkImage4.setImageBitmap(links.get(3).getImage());
					break;
				default:

				}
			}
		else {
			holder.linkImage1.setImageBitmap(null);
			holder.linkImage2.setImageBitmap(null);
			holder.linkImage3.setImageBitmap(null);
			holder.linkImage4.setImageBitmap(null);
		}
		

		return view;

	}

}
