package pt.ipbeja.estig.android.pappsite.adapter;

import java.util.List;

import pt.ipbeja.estig.android.pappsite.R;
import pt.ipbeja.estig.android.pappsite.helper.DatabaseHelper;
import pt.ipbeja.estig.android.pappsite.model.Folder;
import pt.ipbeja.estig.android.pappsite.model.Link;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListLinkAdapter extends ArrayAdapter<Link> {

	private DatabaseHelper db;
	private List<Link> links;
	private Folder folder;
	private LayoutInflater inflater;

	
	/**
	 * Links in a folder Adapter just to list the links in the link_row.xml in the <b>context</b>
	 * 
	 * @param context
	 * @param db
	 * @param folder
	 */
	public ListLinkAdapter(Context context, DatabaseHelper db, Folder folder) {
		super(context, R.layout.link_row);
		this.db = db;
		this.folder = folder;
		inflater = LayoutInflater.from(context);
		refreshLinks();
	}

	public void refreshLinks() {
		links = db.getAllLinksByFolder(folder.getId());
		this.clear();
		for (Link l : links)
			this.add(l);
		this.notifyDataSetChanged();
	}

	static class ViewHolder {
		public TextView linkId;
		public TextView linkTitle;
		public TextView linkUrl;
		public ImageView linkImage;
	}

	@Override
	public int getCount() {
		return links.size();
	}

	@Override
	public Link getItem(int i) {
		return links.get(i);
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {

		ViewHolder holder;

		if (view == null) {
			view = inflater.inflate(R.layout.link_row, viewGroup, false);
			holder = new ViewHolder();
			holder.linkId = (TextView) view.findViewById(R.id.linkId);
			holder.linkTitle = (TextView) view.findViewById(R.id.linkName);
			holder.linkUrl = (TextView) view.findViewById(R.id.linkUrl);
			holder.linkImage = (ImageView) view.findViewById(R.id.linkIcon);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		holder.linkId.setText(Long.toString(getItem(i).getId()));
		holder.linkTitle.setText(getItem(i).getName());
		holder.linkUrl.setText(getItem(i).getUrl());
		holder.linkImage.setImageBitmap(getItem(i).getImage());

		return view;

	}

}
