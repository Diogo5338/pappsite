package pt.ipbeja.estig.android.pappsite.helper;

import java.util.ArrayList;
import java.util.List;

import pt.ipbeja.estig.android.pappsite.model.Folder;
import pt.ipbeja.estig.android.pappsite.model.Link;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	// Database Version
	private static final int DATABASE_VERSION = 2;

	// Database Name
	private static final String DATABASE_NAME = "PAppSite";

	// Table Names
	private static final String TABLE_FOLDER = "folders";
	private static final String TABLE_LINK = "links";

	// Common column names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";

	// LINKS Table - column names
	private static final String KEY_URL = "url";
	private static final String KEY_FOLDER_ID = "folder_id";
	private static final String KEY_IMAGE_BLOB = "image_blob";

	// Table Create Statements
	// Folders table create statement
	private static final String CREATE_TABLE_FOLDERS = "CREATE TABLE "
			+ TABLE_FOLDER + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
			+ " TEXT " + ")";

	// Links table create statement
	private static final String CREATE_TABLE_LINKS = "CREATE TABLE "
			+ TABLE_LINK + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
			+ " TEXT," + KEY_URL + " TEXT," + KEY_FOLDER_ID + " INTEGER,"
			+ KEY_IMAGE_BLOB + " BLOB" + ")";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// creating required tables
		db.execSQL(CREATE_TABLE_FOLDERS);
		db.execSQL(CREATE_TABLE_LINKS);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		String upgradeString = "";

		switch (oldVersion) {
		case 1:
			upgradeString = "ALTER TABLE " + TABLE_LINK + " ADD COLUMN " + KEY_IMAGE_BLOB + "BLOB";
			db.execSQL(upgradeString);
			break;
		default:
			// on upgrade drop older tables
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOLDER);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_LINK);

			// create new tables
			onCreate(db);
		}

	}

	/*
	 * Creating folder
	 */
	public long createFolder(Folder folder) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, folder.getName());

		long id = db.insert(TABLE_FOLDER, null, values);

		return id;
	}

	/*
	 * Creating link
	 */
	public long createLink(Folder folder, Link link) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, link.getName());
		values.put(KEY_URL, link.getUrl());
		values.put(KEY_FOLDER_ID, folder.getId());
		if (link.getImageBytes()!=null)
			values.put(KEY_IMAGE_BLOB, link.getImageBytes());

		long id = db.insert(TABLE_LINK, null, values);

		return id;
	}

	/*
	 * Getting all folders
	 */
	public List<Folder> getAllFolders() {
		List<Folder> folders = new ArrayList<Folder>();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(TABLE_FOLDER, null, null, null, null, null, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				Folder folder = new Folder();
				folder.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				folder.setName((c.getString(c.getColumnIndex(KEY_NAME))));

				// adding to folders list
				folders.add(folder);
			} while (c.moveToNext());
		}

		return folders;
	}

	/*
	 * Getting folder by id
	 */
	public Folder getFolderById(long id) {
		Folder folder = new Folder();

		SQLiteDatabase db = this.getReadableDatabase();

		Cursor c = db.query(TABLE_FOLDER, null, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {

				folder.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				folder.setName((c.getString(c.getColumnIndex(KEY_NAME))));

			} while (c.moveToNext());
		}

		return folder;
	}

	/*
	 * Getting folder by name
	 */
	public Folder getFolderByName(String name) {
		Folder folder = new Folder();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(TABLE_FOLDER, null, KEY_NAME + "=?",
				new String[] { name }, null, null, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {

				folder.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				folder.setName((c.getString(c.getColumnIndex(KEY_NAME))));

			} while (c.moveToNext());
		}

		return folder;
	}

	/*
	 * Getting link by name
	 */
	public Link getLinkByName(String name) {
		Link link = new Link();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(TABLE_LINK, null, KEY_NAME + "=?",
				new String[] { name }, null, null, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {

				link.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				link.setName((c.getString(c.getColumnIndex(KEY_NAME))));
				link.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
				link.setFolderId(c.getInt(c.getColumnIndex(KEY_FOLDER_ID)));
				link.setImage(getBlobFromCursor(c));

			} while (c.moveToNext());
		}

		return link;
	}

	/*
	 * Getting link by url
	 */
	public Link getLinkByUrl(String url) {
		Link link = new Link();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(TABLE_LINK, null, KEY_URL + "=?",
				new String[] { url }, null, null, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {

				link.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				link.setName((c.getString(c.getColumnIndex(KEY_NAME))));
				link.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
				link.setFolderId(c.getInt(c.getColumnIndex(KEY_FOLDER_ID)));
				
				link.setImage(getBlobFromCursor(c));
				
			} while (c.moveToNext());
		}

		return link;
	}

	private byte[] getBlobFromCursor(Cursor c) {
		byte[] blob = null;
		
		try {
			blob = c.getBlob(c.getColumnIndex(KEY_IMAGE_BLOB));
		} catch (Exception e) {
			// keep blob null
		}
		
		return blob;
	}

	/*
	 * Getting link by id
	 */
	public Link getLinkById(long id) {
		Link link = new Link();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(TABLE_LINK, null, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {

				link.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				link.setName((c.getString(c.getColumnIndex(KEY_NAME))));
				link.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
				link.setFolderId(c.getInt(c.getColumnIndex(KEY_FOLDER_ID)));
				link.setImage(getBlobFromCursor(c));

			} while (c.moveToNext());
		}

		return link;
	}

	/*
	 * Getting all links under single folder
	 */
	public List<Link> getAllLinksByFolder(long folder_id) {
		List<Link> links = new ArrayList<Link>();

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.query(TABLE_LINK, null, KEY_FOLDER_ID + "=?",
				new String[] { String.valueOf(folder_id) }, null, null, null);

		// looping through all rows and adding to list
		if (c.moveToFirst()) {
			do {
				Link link = new Link();
				link.setId(c.getInt((c.getColumnIndex(KEY_ID))));
				link.setName((c.getString(c.getColumnIndex(KEY_NAME))));
				link.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
				link.setFolderId(c.getInt(c.getColumnIndex(KEY_FOLDER_ID)));
				link.setImage(getBlobFromCursor(c));

				// adding to links list
				links.add(link);
			} while (c.moveToNext());
		}

		return links;
	}

	/*
	 * Updating a folder
	 */
	public int updateFolder(Folder folder) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, folder.getName());

		// updating row
		return db.update(TABLE_FOLDER, values, KEY_ID + " = ?",
				new String[] { String.valueOf(folder.getId()) });
	}

	/*
	 * Updating a link
	 */
	public int updateLink(Link link) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_NAME, link.getName());
		values.put(KEY_URL, link.getUrl());
		values.put(KEY_FOLDER_ID, link.getFolderId());
		if (link.getImageBytes()!=null)
			values.put(KEY_IMAGE_BLOB, link.getImageBytes());

		// updating row
		return db.update(TABLE_LINK, values, KEY_ID + " = ?",
				new String[] { String.valueOf(link.getId()) });
	}

	/*
	 * Deleting a folder
	 */
	public void deleteFolder(Folder folder, boolean should_delete_all_links) {
		SQLiteDatabase db = this.getWritableDatabase();

		// before deleting folder
		// check if links under this folder should also be deleted
		if (should_delete_all_links) {
			// get all links under this folder
			List<Link> allFolderLinks = getAllLinksByFolder(folder.getId());

			// delete all links
			for (Link link : allFolderLinks) {
				// delete link
				deleteLink(link.getId());
			}
		}

		// now delete the folder
		db.delete(TABLE_FOLDER, KEY_ID + " = ?",
				new String[] { String.valueOf(folder.getId()) });
	}

	/*
	 * Deleting a link
	 */
	public void deleteLink(long link_id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_LINK, KEY_ID + " = ?",
				new String[] { String.valueOf(link_id) });
	}

	/*
	 * Closing database
	 */
	public void closeDB() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null && db.isOpen())
			db.close();
	}

}
