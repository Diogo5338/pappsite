package pt.ipbeja.estig.android.pappsite;

import java.net.URLDecoder;

import pt.ipbeja.estig.android.pappsite.adapter.ListFolderAdapter;
import pt.ipbeja.estig.android.pappsite.helper.DatabaseHelper;
import pt.ipbeja.estig.android.pappsite.model.Folder;
import pt.ipbeja.estig.android.pappsite.model.Link;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private DatabaseHelper db;
	private GridView folderListView;
	private ListFolderAdapter listFolderAdapter;
	private WebView mWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// inflate own content with activity_main.xml resource
		setContentView(R.layout.activity_main);
		// initialize all variables
		db = new DatabaseHelper(getApplicationContext());
		folderListView = (GridView) findViewById(R.id.folderListView);
		folderListView.setSmoothScrollbarEnabled(true);
		folderListView.setStretchMode(GridView.STRETCH_SPACING_UNIFORM);
		listFolderAdapter = new ListFolderAdapter(this, db);
		folderListView.setAdapter(listFolderAdapter);
		folderListView.setClickable(true);
		folderListView.setLongClickable(true);
		folderListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView text = (TextView) view
						.findViewById(R.id.folderRowTextView);
				Folder f = db.getFolderByName(text.getText().toString());
				openLinksActivity(f);
			}
		});
		folderListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0,
							View view, int arg2, long arg3) {
						TextView text = (TextView) view
								.findViewById(R.id.folderRowTextView);
						Folder f = db
								.getFolderByName(text.getText().toString());
						editFolder(f);
						return true;
					}
				});
		
		// import link from an external intent (ex: a link from a browser)
		// at this stage we still obtain the link title using a hidden browser
		// to pass it to the methods of link creation
		if (getIntent().getAction().equals(Intent.ACTION_SEND)) {
			String urlFromIntent = getIntent()
					.getStringExtra(Intent.EXTRA_TEXT);

			mWebView = new WebView(this);
			mWebView.setWebViewClient(new WebViewClient() {
				@Override
				public void onPageFinished(WebView view, String url) {
					String title = view.getTitle();
					if (db.getAllFolders().size() != 0) {
						addNewLinkFromIntent(title, url);
					} else
						addNewFolderAndLinkFromIntent(title, url);

				}
			});
			mWebView.loadUrl(urlFromIntent);
		}

		// import link collection from external intent (ex: from a email)
		if (getIntent().getAction().equals(Intent.ACTION_VIEW)) {

			String urlFromIntent = getIntent().getDataString();
			urlFromIntent = urlFromIntent.replaceFirst(
					LinksActivity.SHARE_SCHEME, "");

			/*
			 * urlFromIntent can be from 2 types:
			 * 	- url/link from type:
			 * 		http://www.google.pt/ (for example)
			 *  - url from scheme type "pappsite": 
			 *  	http://pappsite/<base64 string with folder & links
			 * data>
			 */

			String importString = "";
			try {
				urlFromIntent = URLDecoder.decode(urlFromIntent, "UTF8");
				importString = new String(Base64.decode(urlFromIntent,
						Base64.URL_SAFE), "UTF8");
			} catch (Exception e) {
				
			}

			if (importString.contains("SHARE;"))
				importData(importString);

		}
	}

	/**
	 *  Passes as argument a specific string already
	 *  decoded with folder and link collections to be processed	
	 * 
	 * @param importString
	 */
	private void importData(String importString) {

		// importString = "SHARE;FolderName;Link1::linkurl;Link2::link2url;"
		importString = importString.replaceFirst("SHARE;", "");
		// importString = "FolderName;Link1::linkurl;Link2::link2url;"
		importString = importString.substring(0, importString.length() - 1);
		// importString = "FolderName;Link1::linkurl;Link2::link2url"
		String[] folderData = importString.split(";");

		Folder f = db.getFolderByName(folderData[0]);
		if (f.getName() == null) {
			long folderId = db.createFolder(new Folder(folderData[0]));
			f = db.getFolderById(folderId);
		}

		for (int i = 1; i < folderData.length; i++) {
			String[] linkData = folderData[i].split("::");
			
			Link linkByName = db.getLinkByName(linkData[0]);
			Link linkByUrl = db.getLinkByUrl(linkData[1]);
			
			if (linkByName.getName()==null || linkByUrl.getUrl()==null)
				db.createLink(f, new Link(linkData[0], linkData[1], f.getId(),
						null));
		}

		listFolderAdapter.refreshFolders();

	}

	@Override
	protected void onStop() {
		super.onStop();
		db.closeDB();
		db = null;
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		if (db == null)
			db = new DatabaseHelper(getApplicationContext());
	}

	@Override
	protected void onStart() {
		super.onStart();

		if (db == null)
			db = new DatabaseHelper(getApplicationContext());
	}

	/**
	 * Open Links activity for a specific folder
	 * 
	 * @param f
	 */
	protected void openLinksActivity(Folder f) {
		Intent intent = new Intent(this, LinksActivity.class);
		intent.putExtra(LinksActivity.FOLDER_ID, f.getId());
		startActivity(intent);
	}

	/**
	 * Inflate top right menu with main.xml
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * Specify actions for each menu/button
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_new) {
			addNewFolder();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Add a new link dialog<br>
	 * Note: used only when we get a new intent to add a link 
	 * 
	 * @param title
	 * @param linkUrl
	 */
	private void addNewLinkFromIntent(String title, String linkUrl) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.action_add_link_title);

		// set up the spinner for folder selection
		final Spinner folderList = new Spinner(this);
		
		
		ListFolderAdapter listFolderAdapter = new ListFolderAdapter(this, db, getResources().getString(R.string.action_add_folder_title));
		listFolderAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		folderList.setAdapter(listFolderAdapter);
		
		if (listFolderAdapter.getAllFolders().size()!=0)
			folderList.setSelection(1);
		
		folderList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				Spinner folderList = (Spinner) arg0;
				Folder f = (Folder) folderList.getSelectedItem();

				if (f.getName().equals(
						getResources().getString(
								R.string.action_add_folder_title))) {
					
					// open a new dialog to add a new folder
					addNewFolder(folderList);
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// note used
			}


	    });
		// Set up the input for link name
		final EditText inputTitle = new EditText(this);
		inputTitle.setInputType(InputType.TYPE_CLASS_TEXT);
		inputTitle.setHint(R.string.action_add_link_title_hint);
		inputTitle.setText(title);
		// Set up the input for link url
		final EditText inputUrl = new EditText(this);
		inputUrl.setInputType(InputType.TYPE_CLASS_TEXT);
		inputUrl.setHint(R.string.action_add_link_url_hint);
		inputUrl.setText(linkUrl);
		inputUrl.setEnabled(false);
		
		// Inflate a linearlayout with the two text boxes created above
		LinearLayout content = new LinearLayout(this);
		content.setOrientation(LinearLayout.VERTICAL);
		content.addView(folderList);
		content.addView(inputTitle);
		content.addView(inputUrl);
		
		// Inflate the content of the dialog with the linearlayout
		builder.setView(content);

		// Set up the buttons
		builder.setPositiveButton(R.string.action_add_link,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Link l = new Link();
						Folder f;
						String fName = inputTitle.getText().toString();
						String uName = inputUrl.getText().toString();
						if (fName.equals("") || uName.equals("")) {
							Toast.makeText(MainActivity.this,
									R.string.action_add_link_warning,
									Toast.LENGTH_SHORT).show();
						} else if (db.getLinkByName(fName).getName() != null
								|| db.getLinkByUrl(uName).getUrl() != null) {
							Toast.makeText(MainActivity.this,
									R.string.action_add_link_exists,
									Toast.LENGTH_SHORT).show();
						} else {
							f = (Folder) folderList.getSelectedItem();
							l.setName(fName);
							l.setUrl(uName);
							db.createLink(f, l);
							Intent intent = new Intent(MainActivity.this,
									LinksActivity.class);
							intent.putExtra(LinksActivity.FOLDER_ID, f.getId());
							startActivity(intent);
							mWebView.destroy();
							dialog.dismiss();
						}
					}
				});
		builder.setNegativeButton(R.string.action_cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						mWebView.destroy();
						System.exit(0);
					}
				});

		builder.show();

	}

	/**
	 * Method to add a new link inside of a new folder
	 * 
	 * @param title
	 * @param urlFromIntent
	 */
	private void addNewFolderAndLinkFromIntent(String title,
			String urlFromIntent) {

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.action_add_link_wout_folder_title);

		// Set up the input for folder name
		final EditText inputFolderTitle = new EditText(this);
		inputFolderTitle.setInputType(InputType.TYPE_CLASS_TEXT);
		inputFolderTitle.setHint(R.string.action_add_folder_hint);

		// Set up the input for link name
		final EditText inputTitle = new EditText(this);
		inputTitle.setInputType(InputType.TYPE_CLASS_TEXT);
		inputTitle.setHint(R.string.action_add_link_title_hint);
		inputTitle.setText(title);
		// Set up the input for link url
		final EditText inputUrl = new EditText(this);
		inputUrl.setInputType(InputType.TYPE_CLASS_TEXT);
		inputUrl.setHint(R.string.action_add_link_url_hint);
		inputUrl.setText(urlFromIntent);
		inputUrl.setEnabled(false);
		
		// Inflate a linearlayout with the three text boxes created above
		LinearLayout content = new LinearLayout(this);
		content.setOrientation(LinearLayout.VERTICAL);
		content.addView(inputFolderTitle);
		content.addView(inputTitle);
		content.addView(inputUrl);
		// Inflate the content of the dialog with the linearlayout
		builder.setView(content);

		// Set up the buttons
		builder.setPositiveButton(R.string.action_add_link,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Folder f = new Folder();
						Link l = new Link();
						f.setName(inputFolderTitle.getText().toString());
						l.setName(inputTitle.getText().toString());
						l.setUrl(inputUrl.getText().toString());
						if (f.getName().equals("") || l.getName().equals("")
								|| l.getUrl().equals("")) {
							Toast.makeText(MainActivity.this,
									R.string.action_add_folder_link_warning,
									Toast.LENGTH_SHORT).show();
						} else if (db.getFolderByName(f.getName()).getName() != null
								|| db.getLinkByName(l.getName()).getName() != null
								|| db.getLinkByUrl(l.getUrl()).getUrl() != null) {
							Toast.makeText(MainActivity.this,
									R.string.action_add_folder_link_exists,
									Toast.LENGTH_SHORT).show();
						} else {

							long folderId = db.createFolder(f);
							f.setId(folderId);
							db.createLink(f, l);
							Intent intent = new Intent(MainActivity.this,
									LinksActivity.class);
							intent.putExtra(LinksActivity.FOLDER_ID, f.getId());

							startActivity(intent);
						}
						mWebView.destroy();
						dialog.dismiss();
					}
				});

		builder.setNegativeButton(R.string.action_cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				mWebView.destroy();
				System.exit(0);

			}
		});

		builder.show();

	}
	
	/**
	 * Method to add a new folder from a dialog
	 */
	private void addNewFolder(){
		addNewFolder(null);
	}

	/**
	 * Method to add a new folder from a dialog<br><br>
	 * 
	 * If <b>contextView</b> param is null, it simply will add a new folder
	 * otherwise it will add a folder and refresh the adapter of contextView
	 * to show the new folder added
	 * 
	 * @param contextView
	 */
	private void addNewFolder(final View contextView) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.action_add_folder_title);

		// Set up the input
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		input.setHint(R.string.action_add_folder_hint);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton(R.string.action_add_folder,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Folder f = new Folder();
						String fName = input.getText().toString();
						if (fName.equals("")) {
							Toast.makeText(MainActivity.this,
									R.string.action_add_folder_warning,
									Toast.LENGTH_SHORT).show();
						} else if (db.getFolderByName(fName).getName() != null) {
							Toast.makeText(MainActivity.this,
									R.string.action_add_folder_exists,
									Toast.LENGTH_SHORT).show();
						} else {
							f.setName(fName);
							db.createFolder(f);
							listFolderAdapter.refreshFolders();
							if (contextView!=null) {
								Spinner s = (Spinner) contextView;
								ListFolderAdapter fa = (ListFolderAdapter)s.getAdapter();
								fa.refreshFolders();
								s.setSelection(fa.getCount()-1, true);
							}
							
							dialog.dismiss();
						}
					}
				});
		builder.setNegativeButton(R.string.action_cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();

	}

	/**
	 * Method to edit a folder
	 * 
	 * @param folder
	 */
	protected void editFolder(final Folder folder) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.action_edit_folder_title);

		// Set up the input
		final EditText input = new EditText(this);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		input.setText(folder.getName());
		input.setHint(R.string.action_add_folder_hint);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton(R.string.action_rename_folder,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Folder f = folder;
						String fName = input.getText().toString();
						if (fName.equals("")) {
							Toast.makeText(MainActivity.this,
									R.string.action_add_folder_warning,
									Toast.LENGTH_SHORT).show();
						} else if (db.getFolderByName(fName).getName() != null) {
							Toast.makeText(MainActivity.this,
									R.string.action_add_folder_exists,
									Toast.LENGTH_SHORT).show();
						} else {
							f.setName(fName);
							db.updateFolder(f);
							listFolderAdapter.refreshFolders();
							dialog.dismiss();
						}
					}
				});
		builder.setNeutralButton(R.string.action_delete_folder,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						db.deleteFolder(folder, true);
						listFolderAdapter.refreshFolders();
						dialog.dismiss();
					}
				});
		builder.setNegativeButton(R.string.action_cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();

	}
}
