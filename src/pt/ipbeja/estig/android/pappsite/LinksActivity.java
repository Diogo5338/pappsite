package pt.ipbeja.estig.android.pappsite;

import java.util.List;

import pt.ipbeja.estig.android.pappsite.adapter.ListLinkAdapter;
import pt.ipbeja.estig.android.pappsite.helper.DatabaseHelper;
import pt.ipbeja.estig.android.pappsite.model.Folder;
import pt.ipbeja.estig.android.pappsite.model.Link;
import pt.ipbeja.estig.android.pappsite.utils.Http;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.InputType;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class LinksActivity extends ActionBarActivity {

	public static final String SHARE_SCHEME = "http://pappsite/";
	public static final String FOLDER_ID = "folder_id";

	private DatabaseHelper db;
	private ListView linkListView;
	private ListLinkAdapter listLinkAdapter;
	private Folder folder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// inflate own content with activity_links.xml resource
		setContentView(R.layout.activity_links);
		// initialize all variables
		db = new DatabaseHelper(getApplicationContext());
		folder = db.getFolderById(getIntent().getExtras().getLong(FOLDER_ID));
		this.setTitle(folder.getName());
		listLinkAdapter = new ListLinkAdapter(this, db, folder);
		linkListView = (ListView) findViewById(R.id.linkListView);
		linkListView.setAdapter(listLinkAdapter);
		linkListView.setClickable(true);
		linkListView.setLongClickable(true);
		linkListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				TextView text = (TextView) view.findViewById(R.id.linkId);
				Link l = db.getLinkById(Integer.parseInt(text.getText()
						.toString()));
				String url = l.getUrl();
				if (!url.startsWith("http://") && !url.startsWith("https://"))
					url = "http://" + url;
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		linkListView.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,
					int arg2, long arg3) {
				TextView text = (TextView) view.findViewById(R.id.linkId);
				Link l = db.getLinkById(Integer.parseInt(text.getText()
						.toString()));
				editLink(l);
				return true;
			}

		});
		
		// ensure that all favicons are up to date
		List<Link> links = db.getAllLinksByFolder(folder.getId());
		for (Link l : links)
			updateLinkIcon(l.getId());
		
		
	}
	
	/**
	 * Inflate top right menu with links.xml
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.links, menu);
		return true;
	}
	
	/**
	 * Specify actions for each menu/button
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_new) {
			addNewLink();
			return true;
		} else if (id == R.id.action_share_folder) {
			shareFolder(folder);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Share a folder and all it's content by email<br>
	 * Note: there must be a valid email application configured with
	 * an email account.
	 * 
	 * @param f
	 */
	public void shareFolder(Folder f) {
		
		List<Link> links = db.getAllLinksByFolder(f.getId());
		
		String linksText = "";
		
		for (Link l : links) {
			linksText += l.getName() + "::" + l.getUrl() + ";";
		}
		
		// linksText = "link1::url1;link2::url2;linkN::urlN;"
		
		String textToShare = "SHARE;" + f.getName() + ";" + linksText;
		
		// textToShare = "SHARE;folderName;link1::url1;link2::url2;linkN::urlN;"
			
		try {
			textToShare = SHARE_SCHEME
					+ Base64.encodeToString(textToShare.getBytes("UTF8"),
							Base64.URL_SAFE);
		} catch (Exception e) {
			// nothing to do here
		}
		// textToShare = "http://pappsite/<encodedBase64String(textToShare)>
		
		Intent shareIntent = new Intent(Intent.ACTION_SENDTO);
		shareIntent.setType("text/plain");
		shareIntent.setData(Uri.parse("mailto:"));
		
		String subject = getResources().getString(R.string.action_share_folder);
		String importTitle = getResources().getString(R.string.action_import_folder);
		
		textToShare = "<a href=\"" + textToShare + "\">" + importTitle + ": "
				+ f.getName() + "</a>";
		shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject + ": " + f.getName());
		shareIntent.putExtra(Intent.EXTRA_TEXT, Html
				.fromHtml(textToShare));
		startActivity(Intent.createChooser(shareIntent, subject));
	}

	/**
	 * Add a new link dialog
	 */
	private void addNewLink() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.action_add_link_title);

		// Set up the input for link name
		final EditText inputTitle = new EditText(this);
		inputTitle.setInputType(InputType.TYPE_CLASS_TEXT);
		inputTitle.setHint(R.string.action_add_link_title_hint);
		// Set up the input for link url
		final EditText inputUrl = new EditText(this);
		inputUrl.setInputType(InputType.TYPE_CLASS_TEXT);
		inputUrl.setHint(R.string.action_add_link_url_hint);

		LinearLayout content = new LinearLayout(this);
		content.setOrientation(LinearLayout.VERTICAL);
		content.addView(inputTitle);
		content.addView(inputUrl);

		builder.setView(content);

		// Set up the buttons
		builder.setPositiveButton(R.string.action_add_link,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Link l = new Link();
						String fName = inputTitle.getText().toString();
						String uName = inputUrl.getText().toString();
						if (fName.equals("") || uName.equals("")) {
							Toast.makeText(LinksActivity.this,
									R.string.action_add_link_warning,
									Toast.LENGTH_SHORT).show();
						} else if (db.getLinkByName(fName).getName() != null
								|| db.getLinkByUrl(uName).getUrl() != null) {
							Toast.makeText(LinksActivity.this,
									R.string.action_add_link_exists,
									Toast.LENGTH_SHORT).show();
						} else {
							l.setName(fName);
							l.setUrl(uName);
							long linkId = db.createLink(folder, l);

							updateLinkIcon(linkId);
							listLinkAdapter.refreshLinks();
							dialog.dismiss();
						}
					}
				});
		builder.setNegativeButton(R.string.action_cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();

	}

	
	/**
	 * Launch a asynchronous task to update link favicon
	 * 
	 * @param linkId
	 */
	public void updateLinkIcon(long linkId) {
		new LinksIconsUpdateTask().execute(linkId);

	}

	/**
	 * Implementation of an extended AsyncTask class to update the favicon of a link
	 *
	 */
	public class LinksIconsUpdateTask extends AsyncTask<Long, Long, Long> {
		Bitmap favicon = null;

		@Override
		protected Long doInBackground(Long... linkIds) {

			for (long l : linkIds) {
				Link link = db.getLinkById(l);
				/*
				 * If link.getImage() is null we will try
				 * to get the favicon from internet else
				 * we will define favicon with a default image
				 */
				if (link.getImage() == null) {
					favicon = Http.getFaviconFromURL(link.getUrl());
					if (favicon == null) {
						favicon = ((BitmapDrawable) getResources().getDrawable(
								R.drawable.link_icon_not_available))
								.getBitmap();
					}
					link.setImage(favicon);
				}

				db.updateLink(link);
			}

			return 0L;
		}

		@Override
		protected void onPostExecute(Long result) {
			listLinkAdapter.refreshLinks();
		}
	}

	/**
	 * Edit link dialog
	 * 
	 * @param link
	 */
	protected void editLink(final Link link) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.action_edit_link_title);

		// Set up the input for link name
		final EditText inputTitle = new EditText(this);
		inputTitle.setInputType(InputType.TYPE_CLASS_TEXT);
		inputTitle.setText(link.getName());
		inputTitle.setHint(R.string.action_add_link_title_hint);
		// Set up the input for link url
		final EditText inputUrl = new EditText(this);
		inputUrl.setInputType(InputType.TYPE_CLASS_TEXT);
		inputUrl.setText(link.getUrl());
		inputUrl.setHint(R.string.action_add_link_url_hint);

		LinearLayout content = new LinearLayout(this);
		content.setOrientation(LinearLayout.VERTICAL);
		content.addView(inputTitle);
		content.addView(inputUrl);

		builder.setView(content);

		// Set up the buttons
		builder.setPositiveButton(R.string.action_rename_link,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Link l = link;
						String fName = inputTitle.getText().toString();
						String uName = inputUrl.getText().toString();
						if (fName.equals("") || uName.equals("")) {
							Toast.makeText(LinksActivity.this,
									R.string.action_add_link_warning,
									Toast.LENGTH_SHORT).show();
						} else if ((db.getLinkByName(fName).getName() != null && uName == link
								.getUrl())
								|| (db.getLinkByUrl(uName).getUrl() != null && fName == link
										.getName())) {
							Toast.makeText(LinksActivity.this,
									R.string.action_add_link_exists,
									Toast.LENGTH_SHORT).show();
						} else {
							l.setName(fName);
							l.setUrl(uName);
							db.updateLink(l);
							updateLinkIcon(l.getId());
							listLinkAdapter.refreshLinks();
							dialog.dismiss();
						}
					}
				});
		builder.setNeutralButton(R.string.action_delete_link,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						db.deleteLink(link.getId());
						listLinkAdapter.refreshLinks();
						dialog.dismiss();
					}
				});
		builder.setNegativeButton(R.string.action_cancel,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});

		builder.show();

	}
}
